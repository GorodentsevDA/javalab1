package com.company;

//import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class ClassicPhone extends Device implements ICrudAction {

    String shell;

    ClassicPhone() {
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public void create() {
        goodsCounter += 1;

        this.name = "phoneName";
        this.price = 0;
        this.brand = "brandName";
        this.model = "modelName";
        this.operationSystem = "osName";
        this.shell = "typeShell";
    }

    @Override
    public void read() {
        System.out.println("____________________________________\n");

        System.out.println("Classic phone:\n");
        System.out.println("id: "+id +"\n");
        System.out.println("Имя товара: "+name +"\n");
        System.out.println("Бренд: "+brand +"\n");
        System.out.println("Модель: "+model +"\n");
        System.out.println("ОС: "+operationSystem +"\n");
        System.out.println("Тип корпуса: "+shell +"\n");
        System.out.println("Цена: "+price +"\n");

        System.out.println("____________________________________\n");

    }

    @Override
    public void update() {
        Scanner in = new Scanner(System.in);

        System.out.print("Введите имя товара: ");
        this.name = in.nextLine();

        System.out.print("Введите бренд товара: ");
        this.brand = in.nextLine();

        System.out.print("Введите модель товара: ");
        this.model = in.nextLine();

        System.out.print("Введите ОС товара: ");
        this.operationSystem = in.nextLine();

        System.out.print("Введите тип корпуса товара: ");
        this.shell = in.nextLine();

        System.out.print("Введите цену товара(в рублях): ");
        this.price = in.nextDouble();
    }

    @Override
    public void delete() {
        this.name = null;
        this.price = 0.0;
        this.brand = null;
        this.model = null;
        this.operationSystem = null;
        this.shell = null;

    }

}
