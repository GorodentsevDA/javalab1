package com.company;

public abstract class Device implements ICrudAction {

    static int goodsCounter = 0;

    public String id;
    public String name;
    public double price;
    public String brand;
    public String model;
    public String operationSystem;
}
