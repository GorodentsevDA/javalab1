
package com.company;

import static com.company.Device.goodsCounter;

public class Main {

    public static void main(String[] args) {
        
        int numObjects = Integer.parseInt(args[0]);
        String typeObject = args[1];

        if (numObjects < 1) {
            System.out.println("Неправильное кол-во создаваемых объектов...\n");
            System.exit(1);
        }

        Device[] devices = new Device[numObjects];//объявляем массив объектов

        if (typeObject.equals("ClassicPhone")) {//создание объектов ClassicPhone, заполнение полей объектов
            for(int i = 0; i < numObjects; i++) {
                devices[i] = new ClassicPhone();
                devices[i].create();

                System.out.format("Товар № %d%n", i+1);
                devices[i].update();
            }

        }
        else if(typeObject.equals("SmartPhone")) {//создание объектов SmartPhone, заполнение полей объектов
            for(int i = 0; i < numObjects; i++) {
                devices[i] = new SmartPhone();
                devices[i].create();

                System.out.format("Товар № %d%n", i+1);
                devices[i].update();
            }
        }
        else if(typeObject.equals("Tablet")) {//создание объектов Tablet, заполнение полей объектов
            for(int i = 0; i < numObjects; i++) {
                devices[i] = new Tablet();
                devices[i].create();

                System.out.format("Товар № %d%n", i+1);
                devices[i].update();
            }
        }
        else {
            System.out.println("неверный тип товара...");
            System.exit(1);
        }

        System.out.print("\033[H\033[2J");//очистка экрана консоли
        for(int i = 0; i < numObjects; i++) {//вывод данных каждого объекта в консоль
            devices[i].read();
        }

        System.out.println("Количество товаров: "+goodsCounter);

        for(int i = 0; i < numObjects; i++) {
            devices[i].delete();
            devices[i] = null;//зануление ссылок на объекты для garbage collector-а
        }

        System.gc();//вызов garbage collector-a
    }
}
