package com.company;

interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
